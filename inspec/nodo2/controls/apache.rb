control 'Apache' do
	title 'Tests de Apache'
	desc 'Apache debe estar activado y funcionando'
	describe service('apache2') do
		it { should be_enabled }
		it { should be_installed }
	end
	describe file('/var/www/html') do
		it { should_not be_owned_by('www-data') }
	end 
	describe http(ENV['IP_NODO2']) do
		its('status') { should eq 200 }
		its('headers.Content-Type') { should include 'text/html' }
		its('body') { should include 'Despliegue' }
	end
end
