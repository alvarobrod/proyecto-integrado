control 'Nodo1' do
	title 'Tests del nodo1'
	desc 'El puerto 5432 debe ser alcanzable'
	describe host(ENV['IP_NODO1'], port: 5432, protocol: 'tcp') do
		it { should be_reachable }
	end
	desc 'telnet no debe estar instalado'
	describe package('telnet') do
		it { should_not be_installed }
	end
end
