module "myvm" {
  source               = "../modulos/vm1"
  address_space        = "10.0.0.0/16"
  address_prefix       = "10.0.10.0/24"
  virtual_machine_name = "nodo1"
  private_ip_address   = "10.0.10.4"
}

module "myvm2" {
  source               = "../modulos/vm2"
  virtual_machine_name = "nodo2"
  subnet_id            = module.myvm.subnet_id
  private_ip_address   = "10.0.10.5"
}

resource "null_resource" "ansible1" {
  provisioner "local-exec" {
    command = "ansible-playbook -i ../../ansible/playbooks/nodo1/auth_azure_rm.yml ../../ansible/playbooks/nodo1/site.yml"
  }
  depends_on = [module.myvm]
}

resource "null_resource" "ansible2" {
  provisioner "local-exec" {
    command = "ansible-playbook -i ../../ansible/playbooks/nodo2/auth_azure_rm.yml ../../ansible/playbooks/nodo2/site.yml"
  }
  depends_on = [module.myvm2]
}
